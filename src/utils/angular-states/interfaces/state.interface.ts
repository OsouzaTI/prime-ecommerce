import { Observable } from "rxjs";

interface IState<T> {
    
    getValue : () => T;
    getPrevValue : () => T;
    
    getObservable : () => Observable<T>;
    
    setValue : (value : T) => void;

}

export default IState;