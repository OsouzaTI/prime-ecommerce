import { Observable } from "rxjs";
import { State } from "../state";

interface IStore<T> {

    getObservable : () => Observable<T>;
    getState : () => State<T>;

}

export default IStore;