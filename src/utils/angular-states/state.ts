import { BehaviorSubject, Observable } from "rxjs";
import IState from "./interfaces/state.interface";

export class State<T> implements IState<T> {

    private prevState: T;
    private currState: T;

    // Controladores e detectores
    private stateObservable: Observable<T>;
    private stateBehaviorSubject : BehaviorSubject<T>;

    constructor(initialState : T) {
        this.prevState = initialState;
        this.currState = initialState;
        this.stateBehaviorSubject = new BehaviorSubject(this.getValue());
        this.stateObservable = this.stateBehaviorSubject.asObservable();
    }
    
    public getValue() { return this.currState; }
    public getPrevValue() { return this.prevState; }
    public getObservable() { return this.stateObservable; }
    public setValue(value : T) {
        this.prevState = this.currState;
        this.currState = value;
        // altera o valor do observador, acionando todos os
        // ouvintes registrados no serviço
        this.stateBehaviorSubject.next(this.getValue());
    }

}