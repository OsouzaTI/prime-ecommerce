interface BuildMenuItemProps {
    label: string;
    icon: string;
    items?: BuildMenuItemProps[];
}


export function buildMenuItem(items : BuildMenuItemProps[]) {
    return {...items};
}