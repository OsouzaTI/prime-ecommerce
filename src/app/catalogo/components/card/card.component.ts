import { CurrencyPipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { CarrinhoService } from 'src/app/services/carrinho/carrinho.service';
import { ProdutosService } from 'src/app/services/catalogo/produtos.service';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.less'],
})
export class CardComponent {

  @Input()
  id : number = 0;

  @Input()
  title : string = "";

  @Input()
  subtitle : string = "";

  @Input()
  description : string = "";

  @Input()
  price : number = 0;

  @Input()
  image : string = "";

  @Input()
  styleClass : string = "";

  constructor(
    private carrinhoService : CarrinhoService, 
    private produtosService : ProdutosService,
    private messageService  : MessageService
  ) {}

  adicionarProduto(id : number) {
    const produto = this.produtosService.obter(id);
    if (produto !== undefined) {
      this.carrinhoService.adicionar(produto);  
      this.showSuccess("Produto adicionado ao carrinho com sucesso!");  
    } else {
      this.showError("Erro ao adicionar produto ao carrinho!");
    }
  }


  showSuccess(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Success',
      detail: message 
    });
  }

  showError(message: string) {
    this.messageService.add({
      severity: 'error',
      summary: 'Error',
      detail: message
    });
  }

}
