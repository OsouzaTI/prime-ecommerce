import { Component, Input, OnInit } from '@angular/core';
import { ProdutosService } from 'src/app/services/catalogo/produtos.service';
import { Produtos } from 'src/app/types/catalogo/produto';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})
export class HomeComponent implements OnInit {

  @Input()
  page : number = 1;

  produtos : Produtos = [];

  constructor(private produtosService : ProdutosService) {}
  
  ngOnInit(): void {
    
    // definindo o que sera realizado ao alterar a lista de produtos
    this.produtosService.getObservable().subscribe(res => {
      console.log(res);
      this.produtos = res
    })

    this.produtosService.listar();

  }

}
