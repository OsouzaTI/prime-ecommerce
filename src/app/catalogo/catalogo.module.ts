import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';

import { DataViewModule, DataViewLayoutOptions } from 'primeng/dataview';
import { CardModule } from 'primeng/card';

import { CardComponent } from './components/card/card.component';
import { ButtonModule } from 'primeng/button';

@NgModule({
  declarations: [
    HomeComponent,
    CardComponent,
  ],
  imports: [
    CommonModule,
    DataViewModule,
    CardModule,
    ButtonModule
  ],
  exports: [
    HomeComponent,
    CardComponent
  ]
})
export class CatalogoModule { }
