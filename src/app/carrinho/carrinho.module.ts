import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CarrinhoComponent } from './components/carrinho/carrinho.component';

import { OverlayPanelModule } from 'primeng/overlaypanel';
import { ButtonModule } from 'primeng/button';
import { MiniCardComponent } from './components/mini-card/mini-card.component';


@NgModule({
  declarations: [
    CarrinhoComponent,
    MiniCardComponent,
  ],
  imports: [
    CommonModule,
    OverlayPanelModule,
    ButtonModule,
  ],
  exports: [
    CarrinhoComponent,
    MiniCardComponent,
  ],
})
export class CarrinhoModule { }
