import { Component, OnInit } from '@angular/core';
import { CarrinhoService } from 'src/app/services/carrinho/carrinho.service';
import { Carrinho } from 'src/app/types/carrinho/carrinho';
import { Produtos } from 'src/app/types/catalogo/produto';

@Component({
  selector: 'app-carrinho',
  templateUrl: './carrinho.component.html',
  styleUrls: ['./carrinho.component.less']
})
export class CarrinhoComponent implements OnInit {

  carrinho : Carrinho = {produtos: []};

  constructor(
    private carrinhoService : CarrinhoService,
  ) {}

  ngOnInit(): void {
    this.carrinhoService.getObservable().subscribe((res) => {
      this.carrinho = res;
    })
  }

  produtos() : Produtos {
    return this.carrinho.produtos;
  }

  finalizarCompras() {
    this.carrinhoService.finalizar();
  }

}
