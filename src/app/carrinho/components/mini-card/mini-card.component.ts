import { Component, Input } from '@angular/core';
import { MessageService } from 'primeng/api';
import { CarrinhoService } from 'src/app/services/carrinho/carrinho.service';
import { ProdutosService } from 'src/app/services/catalogo/produtos.service';

@Component({
  selector: 'app-mini-card',
  templateUrl: './mini-card.component.html',
  styleUrls: ['./mini-card.component.less'],
})
export class MiniCardComponent {

  @Input()
  id: number = 0;

  @Input()
  title: string = "";

  @Input()
  price: number = 0;

  constructor(
    private carrinhoService : CarrinhoService,
    private produtosService : ProdutosService,
    private messageService  : MessageService
  ) {}
  
  removerProduto(id : number) {
    const produto = this.produtosService.obter(id);
    if (produto !== undefined) {
      this.carrinhoService.remover(produto);
      this.showSuccess("Produto removido do carrinho com sucesso!");
    }
  }

  showSuccess(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Success',
      detail: message 
    });
  }

}
