import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { CarrinhoService } from 'src/app/services/carrinho/carrinho.service';
import { buildMenuItem } from 'src/utils/menu-helper/menu-helper';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.less']
})
export class NavbarComponent implements OnInit {

  items: MenuItem[] | undefined;

  constructor(private carrinhoService : CarrinhoService) {}

  ngOnInit(): void {
    
    this.items = [
      {
        label: 'Produtos',
        icon: 'pi pi-fw pi-file',
        routerLink: '/'
      },
      {
        label: 'Pagamentos',
        icon: 'pi pi-fw pi-file',
        routerLink: 'pagamentos'
      }
    ];

  }

  gerarTeste() {
    this.carrinhoService.gerarTeste();
  }

}
