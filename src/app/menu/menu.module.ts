import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './components/navbar/navbar.component';

import { MenubarModule } from 'primeng/menubar';
import { InputTextModule } from 'primeng/inputtext';
import { CarrinhoModule } from '../carrinho/carrinho.module';
import { ButtonModule } from 'primeng/button';

@NgModule({
  declarations: [
    NavbarComponent
  ],
  imports: [
    CommonModule,
    MenubarModule,
    InputTextModule,
    CarrinhoModule,
    ButtonModule,
  ],
  exports: [
    NavbarComponent
  ]
})
export class MenuAppModule { }
