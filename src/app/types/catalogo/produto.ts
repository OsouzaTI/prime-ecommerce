export interface Produto {

    id : number;
    nome : string;
    descricao : string;
    imagem: string;
    preco : number;

}

export interface ProdutoResponseDTO {

    id : number;
    title : string;
    description : string;
    image : string;
    price : number;

}

export type Produtos = Produto[];
export type ProdutosResponseDTO = ProdutoResponseDTO[];

function fromDTO(produto: ProdutoResponseDTO) {
    return {
        id: produto.id,
        nome: produto.title,
        descricao: produto.description,
        preco: produto.price,
        imagem: produto.image
    } as Produto;
}

export function fromJson(produtosResponse : ProdutoResponseDTO[]) : Produtos {
    return produtosResponse.map((v, i, a) => fromDTO(v));
}
