import { Produto } from "../catalogo/produto";

export interface Pagamento {
    id: number;
    quantidade: number;
    produto: Produto;
    usuario: {nome: string};
    createdAt: string;    
}
