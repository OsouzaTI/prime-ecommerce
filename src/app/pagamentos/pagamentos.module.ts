import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListaPagamentosComponent } from './lista-pagamentos/lista-pagamentos.component';
import { DataViewModule, DataViewLayoutOptions } from 'primeng/dataview';


@NgModule({
  declarations: [
    ListaPagamentosComponent
  ],
  imports: [
    CommonModule,
    DataViewModule,
  ]
})
export class PagamentosModule { }
