import { Component, OnInit } from '@angular/core';
import { PagamentosService } from 'src/app/services/pagamentos/pagamentos.service';
import { Pagamento } from 'src/app/types/pagamento/pagamento';

@Component({
  selector: 'app-lista-pagamentos',
  templateUrl: './lista-pagamentos.component.html',
  styleUrls: ['./lista-pagamentos.component.less']
})
export class ListaPagamentosComponent implements OnInit {
  
  public pagamentos: Pagamento[] = [];

  constructor(private pagamentosService: PagamentosService) {}

  ngOnInit(): void {
    this.pagamentosService.listar()
      .subscribe(res => {
        console.log(res)
        this.pagamentos = res
      });
  }

}
