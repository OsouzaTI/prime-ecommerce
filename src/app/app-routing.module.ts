import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './catalogo/home/home.component';
import { ListaPagamentosComponent } from './pagamentos/lista-pagamentos/lista-pagamentos.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'pagamentos', component: ListaPagamentosComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
