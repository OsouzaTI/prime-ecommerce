import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Pagamento } from 'src/app/types/pagamento/pagamento';
import { Store } from 'src/utils/angular-states/store';

@Injectable({
  providedIn: 'root'
})
export class PagamentosService extends Store<Pagamento[]> {

  constructor(
    private http : HttpClient,
  ) { 
    super([]);
  }

  listar(): Observable<Pagamento[]> {

    return this.http.get<Pagamento[]>("http://localhost:8080/pagamentos/listar")
      .pipe(res => res, err => err);

  }

}
