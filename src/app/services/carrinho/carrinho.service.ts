import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MessageService } from 'primeng/api';
import { Observable } from 'rxjs';
import { Carrinho } from 'src/app/types/carrinho/carrinho';
import { Produto } from 'src/app/types/catalogo/produto';
import { Store } from 'src/utils/angular-states/store';

@Injectable({
  providedIn: 'root'
})
export class CarrinhoService extends Store<Carrinho> {

  constructor(
    private http : HttpClient,
    private messageService  : MessageService
  ) { 
    super({produtos: []});
  }

  adicionar(produto : Produto) {
    const carrinho = this.getState().getValue();
    this.getState().setValue({
      produtos: [...carrinho.produtos, produto]
    })
  }

  remover(produto : Produto) {
    const carrinho = this.getState().getValue();
    const index = carrinho.produtos.findIndex((p)=>p.id === produto.id);
    if (index !== -1) {
      carrinho.produtos.splice(index, 1);
      this.getState().setValue(carrinho);
    }
  }

  showSuccess(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Success',
      detail: message 
    });
  }

  showError(message: string) {
    this.messageService.add({
      severity: 'error',
      summary: 'Error',
      detail: message
    });
  }

  finalizar() {
    
    let produtos = this.getState().getValue().produtos.map(p => ({
      "apiId": p.id,
      "nome": p.nome,
      "descricao": p.descricao,
      "imagem": p.imagem,
      "preco": p.preco
    }));
    
    const headers = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
    });

    this.http.post("http://localhost:8080/carrinho/comprar", {
      usuarioId: 1,
      produtos: produtos
    }, {headers: headers})
    .subscribe({
      next: console.log, 
      error: () => {
        this.showError('Erro ao realizar a compra, tente novamente mais tarde...');
      }, 
      complete: () => {
        this.showSuccess('Pagamento realizado com sucesso!');
        this.limpar();
      }
    })

  }

  gerarTeste() {

    return this.criarUsuario()
      .subscribe({complete: () => {
        console.log("Usuario criado...");
        this.criarCartao()
          .subscribe({
            error: console.error,
            complete: () => {
              this.showSuccess('Teste criado com sucesso!');
            }
          })
      }})

  }

  private criarCartao() {
    console.log("Criando cartão");
    return this.http.post("http://localhost:8080/cartao/criar", {
      ultimosNumeros: "876",
      valido: true,
      usuario: {
        id: 1
      },
    }).pipe(response => response, error => error);
  }

  private criarUsuario() {
    console.log("Criando usuario");
    return this.http.post("http://localhost:8080/usuario/criar", {
      nome: "Ozéias Silva Souza"
    }).pipe(response => response, error => error);
  }

  limpar() {
    this.getState().setValue({produtos: []});
  }

}
