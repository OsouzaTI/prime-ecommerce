import { HttpClient } from '@angular/common/http';
import { Injectable, OnInit } from '@angular/core';
import { fromJson, Produto, ProdutoResponseDTO, Produtos, ProdutosResponseDTO } from 'src/app/types/catalogo/produto';
import { enviroment } from 'src/env';
import { Store } from 'src/utils/angular-states/store';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProdutosService extends Store<Produtos> {

  private baseUrl : string = enviroment.API_BASE_URL;
  private endPoint = "products";


  constructor(private http : HttpClient) {
    super([]);
  }

  listar() : void {
    this.http.get<ProdutosResponseDTO>(`${this.baseUrl}/${this.endPoint}`)
      .forEach(
        (produtos : ProdutosResponseDTO) => this.getState().setValue(fromJson(produtos))
      );
  }

  obter(id : number) : Produto | undefined {
    const index = this.getState().getValue().findIndex((p) => p.id === id);
    return this.getState().getValue().at(index);
  }

}
